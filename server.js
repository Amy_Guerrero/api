//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./Movimientosv2.json')


app.get('/',function(req, res){
  //res.send('Hola mundo NodeJs');
  res.sendFile(path.join(__dirname,'index.html'));
});
app.get('/Clientes',function(req, res){
  res.send('Aquí están los clientes devueltos nuevos');
});
app.get('/Clientes/:idCliente',function(req, res){
  res.send('Aquí tiene al cliente número : ' + req.params.idCliente);
});
app.get('/Movimientos/v1',function(req, res){
  res.sendfile('Movimientosv1.json');
});
app.get('/Movimientos/v2/:index', function(req, res) {
 console.log(req.params.index);
 res.send(movimientosJSON[req.params.index]);
 //res.sendfile('Movimientosv2.json');
});

app.post('/',function(req,res){
  res.send('Hemos recibido su petición post');
})
